const { mdlinks } = require('../lib/index');
const data = require('./test-mdlinks.json')

test ('succesfully load and import markdown-it-chalis as markdown-it plugin', () => {
    let md = require('markdown-it');
    md.use(mdlinks);
    expect (md.render(data.input)).toBe(data.output);
})
