![This work is Licensed under CC BY 4.0](https://licensebuttons.net/l/by/4.0/80x15.png)
# markdown-it-mdlinks

An extension for `markdown-it` which supports markdown link identification and snippet generation with wiki style links. Inspired by [`markdown-it-wikilinks`](https://github.com/kwvanderlinde/markdown-it-wikilinks).

Goal of the plugin is to allow markdown-it to generate the correct linking tags when parsing
    
- cross-note linking with snippet display (very core)
- embedded video and audio
- graph generation
