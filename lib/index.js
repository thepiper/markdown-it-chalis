"use strict"

// regex for a git hash TODO: revisit when git updates to sha-256
const linkRegExp = /^[a-f0-9]{40}$/i;
var suffix = ".md";
var chalisMode = false


/**
 *
 */
function mdlinks(md, options = {}) {
    if (options.suffix)
    { suffix = options.suffix; }

    if (options.chalisMode === true)
    { suffix = ".chalis.md"; }

    if (options.linkRegExp)
    { linkRegExp = options.linkRegExp; }

    md.inline.ruler.push ('mdlink', mdlink);
}

function mdlink (state) {
    // check start for '[['
    if (state.src.charCodeAt(state.pos) !== 0x5B/* [ */) { return false; }
    if (state.src.charCodeAt(state.pos + 1) !== 0x5B/* [ */) { return false; }

    labelStart = state.pos + 2;
    labelEnd = state.md.helpers.parseLinkLabel(state, state.pos + 1, false);

    // parser failed to find first ']', so it's not a valid link
    if (labelEnd < 0) { return false; }

    // only single ']' found, so it's not a valid link
    if (state.src.charCodeAt(labelEnd + 1) !== 0x5D/* ] */) { return false; }

}

module.exports = {
    mdlinks
}

